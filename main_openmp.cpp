#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

using namespace std;

#define _THRESHOLD_ 0.15
#define _THETA_ 0.5
#define _EPS_ 0.000001

int _K_ = 10;
int _N_ = 200;
int _M_ = 100;

typedef int* data_point;

typedef struct
{
    int *data_point_index;
    int n;
} cluster;

typedef struct
{
    double val;
    int index;
} compare;

//#pragma omp declare reduction(maximum : compare : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

double f(double theta)
{
    return (1.0 - theta) / (1.0 + theta);
}

double sim(const data_point p1, const data_point p2)
{
    int common = 0;
    int total = 0;

    for (int i = 0; i < _N_; i++)
    {
        if (p1[i] && p2[i])
            common++;

        if (p1[i])
            total++;

        if (p2[i])
            total++;
    }

    return double(common) / (double(total) + _EPS_);
}

void compute_links(const data_point *data, int **links)
{
    int i, j, k, c;

    char **is_neighbour = new char *[_M_];
    for (i = 0; i < _M_; i++)
        is_neighbour[i] = new char[_M_];

    #pragma omp parallel for shared(is_neighbour) private(i, j, c)
    for (c = 0; c < _M_ * _M_; c++)
    {
        i = c / _M_;
        j = c % _M_;

        if (i >= j)
        {
            if (sim(data[i], data[j]) > _THRESHOLD_)
                is_neighbour[i][j] = is_neighbour[j][i] = 1;
            else
                is_neighbour[i][j] = is_neighbour[j][i] = 0;
        }
    }

    int common_neighbours;

    #pragma omp parallel for shared(links, is_neighbour) private(i, j, k, c, common_neighbours)
    for (c = 0; c < _M_ * _M_; c++)
    {
        i = c / _M_;
        j = c % _M_;

        if (i >= j)
        {
            common_neighbours = 0;

            for (k = 0; k < _M_; k++)
                if (is_neighbour[i][k] == 1 && is_neighbour[j][k] == 1)
                    common_neighbours++;

            links[i][j] = links[j][i] = common_neighbours;
        }
    }

    for (i = 0; i < _M_; i++)
        delete []is_neighbour[i];

    delete []is_neighbour;
}

int calculate_link_sum(cluster &cluster1, cluster &cluster2, int **links)
{
    int link_number = 0;

    for (int i = 0; i < cluster1.n; i++)
        for (int j = 0; j < cluster2.n; j++)
            link_number += links[cluster1.data_point_index[i]][cluster2.data_point_index[j]];

    return link_number;
}

double calculate_goodness(cluster &cluster1, cluster &cluster2, int **links)
{
    double ln = calculate_link_sum(cluster1, cluster2, links);
    double a = pow(cluster1.n + cluster2.n, 1.0 + 2.0 * f(_THETA_));
    double b = pow(cluster1.n, 1.0 + 2.0 * f(_THETA_));
    double c = pow(cluster2.n, 1.0 + 2.0 * f(_THETA_));
    return ln / (a - b - c);
}

void merge_clusters(const int i, const int j, cluster clusters[], int cluster_number)
{
    cluster &cluster1 = clusters[i];
    cluster &cluster2 = clusters[j];
    int *new_data_point_index = new int[cluster1.n + cluster2.n];

    for (int i = 0; i < cluster1.n; i++)
        new_data_point_index[i] = cluster1.data_point_index[i];

    for (int i = 0; i < cluster2.n; i++)
        new_data_point_index[cluster1.n + i] = cluster2.data_point_index[i];

    cluster1.n += cluster2.n;
    delete []cluster1.data_point_index;
    cluster1.data_point_index = new_data_point_index;

    int last = cluster_number - 1;
    if (last != j)
    {
        delete []cluster2.data_point_index;
        cluster2.data_point_index = clusters[last].data_point_index;
        cluster2.n = clusters[last].n;
    }
    else
    {
        // nothing
    }
}

double test_rock()
{
    data_point *points = new data_point[_M_];
    for (int i = 0; i < _M_; i++)
        points[i] = new int[_N_];

    int **links = new int*[_M_];
    for (int i = 0; i < _M_; i++)
        links[i] = new int[_M_];

    ifstream input("data.txt", ios_base::in);
    for (int i = 0; i < _M_; i++)
        for (int j = 0; j < _N_; j++)
            input >> points[i][j];
    input.close();

    clock_t c1 = clock();

    compute_links(points, links);

    cluster *clusters = new cluster[_M_];
    int cluster_number = _M_;
    for (int i = 0; i < _M_; i++)
    {
        clusters[i].data_point_index = new int[1];
        clusters[i].data_point_index[0] = i;
        clusters[i].n = 1;
    }

    int merge_iterations = cluster_number - _K_;
    for (int cl = 0; cl < merge_iterations; cl++)
    {
        int c, max_c = 0;
        double max_goodness = -1.0;
        compare max_comp;
        max_comp.val = 0.0;
        max_comp.index = 0;

        #pragma omp parallel for default(none) shared(clusters, cluster_number, max_comp, links) private(c)
        //#pragma omp parallel for reduction(maximum : max_comp)
        for (c = 0; c < cluster_number * cluster_number; c++)
        {
            int i, j;

            i = c / cluster_number;
            j = c % cluster_number;

            if (i > j)
            {
                double goodness = calculate_goodness(clusters[i], clusters[j], links);

                #pragma omp critical
                {
                    if (goodness > max_comp.val)
                    {
                        max_comp.index = c;
                        max_comp.val = goodness;
                    }
                }
            }
        };

        int max_i = max_comp.index / cluster_number;
        int max_j = max_comp.index % cluster_number;

        merge_clusters(max_i, max_j, clusters, cluster_number);

        cluster_number--;
    }

    clock_t c2 = clock();

    for (int i = 0; i < _M_; i++)
        delete []points[i];
    delete []points;

    for (int i = 0; i < _M_; i++)
        delete []links[i];
    delete []links;

    for (int i = 0; i < cluster_number; i++)
        delete []clusters[i].data_point_index;
    delete []clusters;

    return double(c2 - c1) / CLOCKS_PER_SEC;
}

int main()
{
    ofstream f("results.txt");

    for (int threads = 1; threads <= 8; threads++)
    {
        for (int num = 500; num <= 1500; num += 100)
        {
            omp_set_num_threads(threads);
            _M_ = num;

            double t;

            cout << "Threads=" << threads << " M=" << num << " time=";

            t = test_rock();

            cout << t << endl;

            f << t << " ";
        }

        f << endl;
    }

    f.close();

    return 0;
}
