#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "mpi.h"

using namespace std;

#define _THRESHOLD_ 0.15
#define _THETA_ 0.5
#define _EPS_ 0.000001

int _K_ = 10;
int _N_ = 200;
int _M_ = 100;

typedef int* data_point;

typedef struct
{
    int *data_point_index;
    int n;
} cluster;

typedef struct
{
    double val;
    int index;
} compare;

double f(double theta)
{
    return (1.0 - theta) / (1.0 + theta);
}

double sim(const data_point p1, const data_point p2)
{
    int common = 0;
    int total = 0;

    for (int i = 0; i < _N_; i++)
    {
        if (p1[i] && p2[i])
            common++;

        if (p1[i])
            total++;

        if (p2[i])
            total++;
    }

    return double(common) / (double(total) + _EPS_);
}

void compute_links(const data_point *data, int *links)
{
    char *is_neighbour = new char[_M_ * _M_];
    int elnum = _M_ * _M_;
    
    int mpi_rank, mpi_size;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    
    int block_size = elnum / mpi_size;    
    int start_index = mpi_rank * block_size;
    int end_index = (mpi_rank + 1) * block_size;
    
    if (end_index > elnum)
        end_index = elnum;
        
    char local_block1[end_index - start_index];
    for (int c = start_index; c < end_index; c++)
    {
        int i = c / _M_;
        int j = c % _M_;

        if (sim(data[i], data[j]) > _THRESHOLD_)
            local_block1[c - start_index] = 1;
        else
            local_block1[c - start_index] = 0;
    }
    
    MPI_Allgather(local_block1,  end_index - start_index, MPI_CHAR,
                  is_neighbour, end_index - start_index, MPI_CHAR,
                  MPI_COMM_WORLD);

    int local_block2[end_index - start_index];
    for (int c = start_index; c < end_index; c++)
    {
        int i = c / _M_;
        int j = c % _M_;

        int common_neighbours = 0;

        for (int k = 0; k < _M_; k++)
            if (is_neighbour[i * _M_ + k] == 1 && is_neighbour[j * _M_ + k] == 1)
                common_neighbours++;

        local_block2[c - start_index] = common_neighbours;
    }
    
    MPI_Allgather(local_block2, end_index - start_index, MPI_INT,
                  links,        end_index - start_index, MPI_INT,
                  MPI_COMM_WORLD);}

int calculate_link_sum(cluster &cluster1, cluster &cluster2, int *links)
{
    int link_number = 0;

    for (int i = 0; i < cluster1.n; i++)
        for (int j = 0; j < cluster2.n; j++)
        {
            int links_index = cluster1.data_point_index[i] * _M_ + cluster2.data_point_index[j];
        
            link_number += links[links_index];
        }

    return link_number;
}

double calculate_goodness(cluster &cluster1, cluster &cluster2, int *links)
{
    double ln = calculate_link_sum(cluster1, cluster2, links);
    double a = pow(cluster1.n + cluster2.n, 1.0 + 2.0 * f(_THETA_));
    double b = pow(cluster1.n, 1.0 + 2.0 * f(_THETA_));
    double c = pow(cluster2.n, 1.0 + 2.0 * f(_THETA_));
    return ln / (a - b - c);
}

void merge_clusters(const int i, const int j, cluster clusters[], int cluster_number)
{
    cluster &cluster1 = clusters[i];
    cluster &cluster2 = clusters[j];
    int *new_data_point_index = new int[cluster1.n + cluster2.n];

    for (int i = 0; i < cluster1.n; i++)
        new_data_point_index[i] = cluster1.data_point_index[i];

    for (int i = 0; i < cluster2.n; i++)
        new_data_point_index[cluster1.n + i] = cluster2.data_point_index[i];

    cluster1.n += cluster2.n;
    delete []cluster1.data_point_index;
    cluster1.data_point_index = new_data_point_index;

    int last = cluster_number - 1;
    if (last != j)
    {
        delete []cluster2.data_point_index;
        cluster2.data_point_index = clusters[last].data_point_index;
        cluster2.n = clusters[last].n;
    }
    else
    {
        // nothing
    }
}

double test_rock()
{
    data_point *points = new data_point[_M_];
    for (int i = 0; i < _M_; i++)
        points[i] = new int[_N_];

    int *links = new int[_M_ * _M_];
    //for (int i = 0; i < _M_; i++)
    //    links[i] = new int[_M_];

    ifstream input("data.txt", ios_base::in);
    for (int i = 0; i < _M_; i++)
        for (int j = 0; j < _N_; j++)
            input >> points[i][j];
    input.close();

    clock_t c1 = clock();

    compute_links(points, links);

    cluster *clusters = new cluster[_M_];
    int cluster_number = _M_;
    for (int i = 0; i < _M_; i++)
    {
        clusters[i].data_point_index = new int[1];
        clusters[i].data_point_index[0] = i;
        clusters[i].n = 1;
    }

    int merge_iterations = cluster_number - _K_;
    for (int cl = 0; cl < merge_iterations; cl++)
    {   
        int mpi_rank, mpi_size;
        MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
        
        int elnum = cluster_number * cluster_number;
        int block_size = elnum / mpi_size;
        int start_index = mpi_rank * block_size;
        int end_index = (mpi_rank + 1) * block_size;
        
        if (end_index > elnum)
            end_index = elnum;
            
        int local_max_c = 0;
        double local_max_goodness = -1.0;
        int all_max_c[mpi_size];
        double all_max_goodness[mpi_size];

        for (int c = 0; c < elnum; c++)
        {
            int i = c / cluster_number;
            int j = c % cluster_number;
            
            if (i == j)
                continue;

            double goodness = calculate_goodness(clusters[i], clusters[j], links);

            if (goodness > local_max_goodness)
            {
                local_max_c = c;
                local_max_goodness = goodness;
            }
        }
        
        MPI_Allgather(&local_max_c, 1, MPI_INT,
                       all_max_c,   1, MPI_INT,
                       MPI_COMM_WORLD);
        MPI_Allgather(&local_max_goodness, 1, MPI_DOUBLE,
                       all_max_goodness,   1, MPI_DOUBLE,
                       MPI_COMM_WORLD);
        
        int total_max_c = 0;
        double total_max_goodness = -1.0;
        
        for (int k = 0; k < mpi_size; k++)
            if (all_max_goodness[k] > total_max_goodness)
            {
                total_max_goodness = all_max_goodness[k];
                total_max_c = all_max_c[k];
            }

        int max_i = total_max_c / cluster_number;
        int max_j = total_max_c % cluster_number;

        merge_clusters(max_i, max_j, clusters, cluster_number);

        cluster_number--;
    }

    clock_t c2 = clock();
    /*
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank == 0)
    {
        for (int i = 0; i < cluster_number; i++)
        {
            for (int j = 0; j < clusters[i].n; j++)
                cout << clusters[i].data_point_index[j] << " ";

            cout << endl;
        }
    }
    */
    for (int i = 0; i < _M_; i++)
        delete []points[i];
    delete []points;

    //for (int i = 0; i < _M_; i++)
    //    delete []links[i];
    delete []links;

    for (int i = 0; i < cluster_number; i++)
        delete []clusters[i].data_point_index;
    delete []clusters;

    return double(c2 - c1) / CLOCKS_PER_SEC;
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);
    
    int mpi_rank, mpi_size;
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    
    if (mpi_rank == 0)
        cout << "Size: " << mpi_size << ", time: ";

    for (int data_size = 100; data_size <= 1000; data_size += 100)
    {
        _M_ = data_size;

        double t = test_rock();
        
        if (mpi_rank == 0)
            cout << t << " ";
    }
        
    if (mpi_rank == 0)
        cout << endl;
    
    MPI_Finalize();

    return 0;
}
